import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

import HomePage from './HomePage/HomePage.js';
import TablePage from './TablePage/TablePage.js';
import ManagementSupervisorsPage from './ManagementSupervisors/Component/ManagementSupervisors.js';

const Error = () => {
  return (
    <div>
      <h1>path is dose not exist </h1>
    </div>
  );

};


class Routering extends Component {


  render() {

    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path='/' component={HomePage} key='/' />
            <Route path='/table' component={TablePage} key='1'  />
            <Route path='/supervisors' component={ManagementSupervisorsPage} key='2' />
            <Route component={Error} key='3'/>
          </Switch>
        </BrowserRouter>
      </div>

    );
  }
}


export default Routering;
