import React from 'react';
import NavbarMenu from '../../HomePage/Navigation.js';
import TableSupervisors from './TableSupervisors.js';
import NavCard from './CardSupervisors.js';
class ManagementSupervisorsPage extends React.Component
{
  constructor() {
    super();
    this.state = {
      apiRsesponse: this.callAPI(),
    };
  }
  // NOTE: all the data call from here and pass to TableSupervisors
  callAPI() {
    fetch('http://localhost:3005/supervisors')
      .then(res => res.text())
      .then(res => {
        this.setState({ apiResponse: JSON.parse(res) });
      });
  }

  render () {
    // NOTE: when the this in const isnt change and the data rendering good we pass the data to CardSupervisors as props, we need to return the new table after change
    const bla = () => {
      this.callAPI();
    };
    return (
      <div>
        <div>
          <NavbarMenu/>
        </div>
        <div>
          <TableSupervisors tableData = {this.state.apiResponse}/>
        </div>
        <div>
          <NavCard action = {bla}/>
        </div>

      </div>
    );
  }
}
export default ManagementSupervisorsPage;
