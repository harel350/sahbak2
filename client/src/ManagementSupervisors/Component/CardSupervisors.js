import React from 'react';
import axios from 'axios';
import { MDBBtn, MDBCard, MDBCardTitle, MDBCollapse} from 'mdbreact';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import '../CssComponent/CardSupervisors.css';


class NavCard extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      collapseId : ' ',
      bla : this.props.action

    };

  }
  toggleCollapse  (collapseID) {
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));
  }
  render(){

    return (
      <MDBCard className = "AddSupervisors">
        <MDBCardTitle>
          <MDBBtn color="primary" onClick={()=> this.toggleCollapse('deleteCollapse')} style={{ marginBottom: '1rem' }}>"מחיקת מנחה"</MDBBtn>
          <MDBBtn color="primary" onClick={()=> this.toggleCollapse('addCollapse')} style={{ marginBottom: '1rem' }}>"הוספת מנחה"</MDBBtn>
          <MDBCollapse id="addCollapse" isOpen={this.state.collapseID}>
            <AddSupervisors action={this.props.action}/>
          </MDBCollapse>

          <MDBCollapse id="deleteCollapse" isOpen={this.state.collapseID}>
            <DeleteSupervisors action={this.props.action}/>
          </MDBCollapse>

        </MDBCardTitle>

      </MDBCard>

    );
  }
}

class AddSupervisors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {NameValue: '', IdValue: '', ApproveValue: false , Note : ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkid = this.checkid.bind(this);
    this.restart = this.restart.bind(this);
  }

  checkid(event) {
    let check = event.target.value;
    parseInt(check,10);

    this.setState({IdValue : check});
  }
  restart(){
    this.setState({
      NameValue: '', IdValue: '', ApproveValue: false , Note : ''
    });
  }
  handleChange(event) {
    switch (event.target.name) {
    case 'nameSupervisor':
      this.setState({NameValue: event.target.value});
      break;
    case 'idSupervisor':
      this.checkid(event);
      break;
    case 'approveSupervisor':
      this.setState({ApproveValue: event.target.checked});
      break;
    case 'noteSupervisor':
      this.setState({Note: event.target.value});
      break;
    default:

    }



  }
  handleSubmit(event) {
    event.preventDefault();

    const addSupervisors = {
      nameSupervisor    : this.state.NameValue,
      idSupervisor      : this.state.IdValue,
      approveSupervisor : this.state.ApproveValue,
      note              : this.state.Note
    };
    var config = {
      headers: {'Access-Control-Allow-Origin': '*'}
    };

    axios.post('http://localhost:3005/supervisors/add', addSupervisors, config)
    /*  .then(res => {
        console.log('this is res.data',res.data);
      })*/.then(res => {
        this.props.action();
      }).then(this.restart());//.catch(err => console.log(err));


  }
  render(){
    return(
      <form onSubmit={this.handleSubmit}>
        <label id="nameSupervisor">
          <span>:שם מנחה</span>
          <input type="text" name="nameSupervisor" value={this.state.NameValue} onChange={this.handleChange}/>
        </label><br/>
        <label id = "idSupervisor">
          <span>:תז</span>
          <input type="text" name="idSupervisor" value={this.state.IdValue} onChange={this.handleChange}/>
        </label><br/>
        <label id = "approveSupervisor">
          <span>:מאושר</span>
          <input type="checkbox" name="approveSupervisor" value={this.state.ApproveValue} onChange={this.handleChange}/>
        </label><br/>
        <label id = "noteSupervisor">
          <span>:הערות</span>
          <input type="textarea" name="noteSupervisor" value={this.state.Note} onChange={this.handleChange}/>
        </label><br/>
        <input type="submit" value="הוסף" />
      </form>



    );
  }
}

class DeleteSupervisors extends React.Component{
  constructor(props){
    super(props);
    this.state = {NameValue : '', IdValue : ''};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleSubmit(event){

    event.preventDefault();
    var deleteSupervisors ={};
    var config = {
      headers: {'Access-Control-Allow-Origin': '*'}
    };

    if(this.state.NameValue !== '' && this.state.IdValue === ''){
      deleteSupervisors = { nameorid : this.state.NameValue};
      axios.post('http://localhost:3005/supervisors/deletename',deleteSupervisors, config)
        .then(res => {
          Swal.fire(res.data);
        }).then(res => {
          this.props.action();
        }).then(this.setState({NameValue : ''}));//.catch(err => console.log(err));

    }
    if(this.state.NameValue === '' && this.state.IdValue !== ''){
      deleteSupervisors = { nameorid : this.state.IdValue};
      axios.post('http://localhost:3005/edit/deleteid',deleteSupervisors, config)
      /*  .then(res => {
          console.log('this is res.data',res.data);
        })*/.then(res => {
          this.props.action();
        }).then(this.setState({IdValue : ' '}));//.catch(err => console.log(err));

    }

  }
  handleChange(event) {
    switch (event.target.name) {
    case 'nameSupervisor':
      this.setState({NameValue: event.target.value});
      break;
    case 'idSupervisor':
      this.setState({IdValue: event.target.value});
      break;
    default:

    }
  }

  render(){
    return(
      <form onSubmit={this.handleSubmit}>
        <span>:הזן שם מנחה או תז למחיקה</span>
        <label id="nameSupervisor">
          <span>:שם מנחה</span>
          <input type="text" name="nameSupervisor" value={this.state.NameValue} onChange= {this.handleChange}/>
        </label><br/>
        <label id = "idSupervisor">
          <span>:תז</span>
          <input type="text" name="idSupervisor" value={this.state.IdValue} onChange = {this.handleChange}/>
        </label><br/>
        <input type="submit" value="מחק" />
      </form>

    );
  }
}



export default NavCard;
