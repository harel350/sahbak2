import React from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
//import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import '../CssComponent/TableSupervisors.css';
class TableSupervisors extends React.Component
{
  render(){

    return(
      <div className = "SupervisorsTable">
        <BootstrapTable data={ this.props.tableData}>
          <TableHeaderColumn dataField='note' isKey>הערות</TableHeaderColumn>
          <TableHeaderColumn dataField='approved'>מאושר</TableHeaderColumn>
          <TableHeaderColumn dataField='namementor'>שם מנחה</TableHeaderColumn>
          <TableHeaderColumn dataField='idmentor'>תז מנחה</TableHeaderColumn>

        </BootstrapTable>
      </div>
    );
  }
}
export default TableSupervisors;
