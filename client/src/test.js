import React from 'react';
import {addArticle} from './js/actions/index.js';
import index from './js/index.js';
import store from './js/store/index';

class Testing extends React.Component{
  render(){
    console.log('test1', store.getState());
    store.subscribe(() => console.log('Look ma, Redux!!'));
    store.dispatch(addArticle({ title: 'React Redux Tutorial for Beginners', id: 1 }) );
    console.log('test2', store.getState());
    store.subscribe(() => console.log('Look ma1, Redux!!'));
    store.dispatch(addArticle({ title: 'React Redux Tutorial for Beginners', id: 1 }) );
    console.log('test3', store.getState().articles);
    return(
      <h1>fdfs</h1>
    );
  }
}
export default Testing;
