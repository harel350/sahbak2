import React from 'react';
import axios from 'axios';
import './CssComponent/Table.css';
import CurrentTable from './currentTable.js';
import { Link, withRouter } from 'react-router-dom';
import {MDBCollapse} from 'mdbreact';

class Table extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      numberOfTable: 0, // number of the table show
      colorTable: 'BA67AF', // color of the table
      currentUrl: '/project', // current url
      collapseId : ' ',
      apiResponse: [[]],
    };

    this.toggleCollapse = this.toggleCollapse.bind(this);

  }

  componentDidUpdate(prevProps,prevState){

    var config = {
      headers: {'Access-Control-Allow-Origin': '*'}
    };
    if(prevState.apiResponse===this.state.apiResponse && this.state.currentUrl!=='/testfilter'){
      fetch('http://localhost:3005/table'+this.state.currentUrl,config)
        .then(res => res.text())
        .then(res => this.setState({ apiResponse: JSON.parse(res)}));
    }
  }
  // set the color of the table according to the index of the table
  setColor (id) {
    switch (id) {
    case 1:
      return '#26c6da';

    case 2:
      return '#BA67AF';

    case 3:
      return '#E0868F';

    case 4:
      return '#007bFF';

    default:
      return '#EF5350';
    }
  }
  // set the state of number table, color of the table and the current url
  setTable (idCurrentTable) {
    let color = this.setColor(idCurrentTable);
    this.setState({
      numberOfTable: idCurrentTable,
      colorTable: color,

    });
  }
  toggleCollapse  (collapseID, idCurrentTable, currentUrl1) {
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : '',
      currentUrl : currentUrl1
    }));
    this.setTable(idCurrentTable);
  }

  render () {

    return (

      <div id="body">
        <div className="row">


          <div className="col-md-2">
            <Link to= '/table/test'>
              <div className="card-counter primary" onClick = {() =>this.toggleCollapse('needtest',1,'/test')}>
                <i className="fa fa-superscript"></i>
                <span className="count-numbers">12</span>
                <span className="count-name">צריכים להיבחן</span>
              </div>
            </Link>
          </div>
          <div className="col-md-2">
            <Link to= '/table/diploma'>
              <div className="card-counter info" onClick = {() => this.toggleCollapse('',4,'/diploma')} >
                <i className="fa fa-graduation-cap"></i>
                <span className="count-numbers">35</span>
                <span className="count-name">הנפקת דיפלומה</span>
              </div>
            </Link>
          </div>

          <div className="col-md-2">
            <Link to= '/table/defense'>
              <div className="card-counter danger" onClick = {() => this.toggleCollapse('',2,'/defense')}>
                <i className="glyphicon glyphicon-time"></i>
                <span className="count-numbers">599</span>
                <span className="count-name" >ממתנים להגנה</span>
              </div>
            </Link>
          </div>

          <div className="col-md-2">
            <Link to= '/table/project'>
              <div className="card-counter success" onClick = {() => this.toggleCollapse('',3,'/project')}>
                <i className="fa fa-book"></i>
                <span className="count-numbers">{this.state.apiResponse.length}</span>
                <span className="count-name" >הגישו פרויקט</span>
              </div>
            </Link>
          </div>

          <div className="col-md-2">
            <Link to = '/table'>
              <div className="card-counter warning" onClick = {() => this.toggleCollapse('',0,'/')} >
                <i className="fa fa-home"></i>
                <span className="count-numbers">670</span>
                <span className="count-name">טבלה ראשית</span>
              </div>
            </Link>
          </div>


        </div>
        <div dir="rtl">
          <MDBCollapse id="needtest" isOpen={this.state.collapseID}>
            <TestFilter />
          </MDBCollapse>
        </div>
        <div>
          <CurrentTable numTable={this.state.numberOfTable} colorTable={this.state.colorTable} apiResponse={this.state.apiResponse} />
        </div>

      </div>

    );
  }
}

class TestFilter extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      test_symbol : '',
      date_from : '01/01/1900',
      date_until: '30/12/2100',
      data : [[]]
    };
    this.handelChange = this.handelChange.bind(this);
    this.handelSubmit = this.handelSubmit.bind(this);
  }
  handelChange(event){

    switch (event.target.name) {
    case 'subject':
      this.setState({test_symbol: event.target.value});
      break;
    case 'date_from':
      this.setState({date_from  : event.target.value});
      break;
    case 'date_until':
      this.setState({date_until : event.target.value});
      break;
    default:
      break;
    }

  }
  handelSubmit(event){
    event.preventDefault();

    const testFilter = {
      test_symbol : this.state.test_symbol,
      date_from   : this.state.date_from,
      date_until  : this.state.date_until
    };
    var config = {
      headers: {'Access-Control-Allow-Origin': '*'}
    };
    axios.post('http://localhost:3005/table/testfilter',testFilter,config)
      .then(res => {
        this.setState({data:res.data});
      });



  }
  render(){
    return(
      <div>
        <form id="filter_for_test" onSubmit={this.handelSubmit}>
          <label>בחר מקצוע :
            <select name="subject" onChange={this.handelChange}>
              <option value='94613' name="94613">ארגון קבצים</option>
              <option value="94611" name="94611">סטטיסטיקה</option>
              <option value="94621" name="94621">אסמבלר</option>
              <option value="94623" name="94623">שפת C</option>
            </select>
          </label>
          <label>הכנס תאריך מ -
            <input type="date" name='date_from' onChange={this.handelChange}/>
          </label>
          <label> עד -
            <input type="date" name='date_until' onChange={this.handelChange}/>
          </label>
          <input type="submit" value="אישור"/>
        </form>
        

      </div>
    );
  }
}


export default Table;
