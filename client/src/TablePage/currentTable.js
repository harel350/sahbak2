import React from 'react';
import './CssComponent/CurrentTable.css';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';


export default class CurrentTable extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      TableHeaderColumn : [
        ['id','n_a','n_p','fullname','dateofdefense','dateofsendmahat'],
        ['fullname','id','date','name_test'],
        ['id','n_a','n_p','fullname','dateofdefense','dateofsendmahat'],
        ['id','n_a','n_p','fullname','phone','projectname','namementor','idmentor','approved','interface','dateofsubmission','dateofsendmahat','dateofbackfrommahat','note'],
        ['id','n_a','n_p','fullname','dateofdefense','dateofsendmahat','dateofbackfrommahat','note']
      ],
      Table: [
        [ 'תז', 'מספר פעולה', 'מספר אישי', 'שם מלא', 'סטטוס', 'תת - סטטוס'],
        ['שם','תז','תאריך','שם מבחן'],
        [ 'תז', 'מספר פעולה', 'שם מלא', 'טלפון', 'ת.אישור פרויקט', 'תאריך הגנה'],
        [ 'תז', 'מספר פעולה', 'מספר אישי', 'שם מלא', 'טלפון', 'פרויקט', 'שם מנחה', 'תז מנחה', 'מנחה מאושר', 'ממשק', 'ת.הגשה', 'נשלח למהט', 'חזר ממהט', 'הערות'],
        [ 'תז', 'מספר פעולה', 'מספר אישי', 'שם מלא', 'ת.הגנה', 'ת.שליחה למהט', 'ת.חזר ממהט', 'הערות']
      ],

    };

  }

  render () {
    console.log('type of :  ',typeof(this.props.apiResponse));
    return (
      <div dir="rtl">
        <BootstrapTable data={this.props.apiResponse}>
          {
            this.state.Table[this.props.numTable].map((x,index) =>{
              let key = index===1 ? true:false;
              return(
                <TableHeaderColumn dataField={this.state.TableHeaderColumn[this.props.numTable][index]} isKey={key}  key={index}>{x}</TableHeaderColumn>);
            })
          }
        </BootstrapTable>
      </div>
    );
  }
}
