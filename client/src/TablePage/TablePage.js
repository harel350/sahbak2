import React from 'react';
import NavbarMenu from '../HomePage/Navigation.js';
import Table from './table.js';
class TablePage extends React.Component

{
  render () {
    return (

      <div>
        <div>
          <NavbarMenu/>
        </div>
        <div>
          <Table/>
        </div>
      </div>
    );
  }
}
export default TablePage;
