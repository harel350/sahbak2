import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routering from './Router.js';
import registerServiceWorker from './registerServiceWorker';
import Testing from './test.js';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import index from './js/index.js';
// import Swal from 'sweetalert2';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';




ReactDOM.render(<Routering/>, document.getElementById('root'));
registerServiceWorker();
