import { ADD_ARTICLE } from '../constants/action-types.js';
const initialState = {
  articles: [],
  //testing : []
};
function rootReducer(state = initialState, action) {
  if (action.type === ADD_ARTICLE) {
    return Object.assign({}, state, {
      articles : state.articles.concat(action.payload)
    //  testing : state.testing.concat(action.payload)
    });
  }
  return state;
}
export default rootReducer;
