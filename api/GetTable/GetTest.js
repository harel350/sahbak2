///.var express = require('express');
var MyClient = require('../connectionDB.js');

const startTable = `SELECT student.firstname || ' ' || student.lastname as fullname, student.id, test_date.date, test.name_test
FROM main.student
JOIN main.student_test ON student.studentid = student_test.student_id
JOIN main.test_date ON student_test.test_symbol = test_date.test_symbol AND student_test.date = test_date.date
JOIN main.test ON test.test_symbol = test_date.test_symbol
`


function getStartTable(callback) {
       MyClient.query(startTable, function(err, res, fields) {
           callback(err, res);
       });
   }


   module.exports = {
       getStartTable     : getStartTable,


   }
