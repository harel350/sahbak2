var express = require('express');
var router = express.Router();
var Mentor = require('../GetTable/GetMentor');
var MyClient = require('../connectionDB.js');
const R = require('ramda');


//show the table of the supervisors
router.get('/', function(req, res, next) {
      Mentor.getData(function(err, result) {

          res.send(result.rows)
        });
});

//query to add new supervisors to the db
router.post('/add', function(req, res) {
      res.send(req.body.note);
      var approve = req.body.approveSupervisor ? 'v':'X';
      MyClient.query(`INSERT INTO main.mentor (idmentor,namementor,approved,note)
                     VALUES ('${req.body.idSupervisor}','${req.body.nameSupervisor}','${approve}','${req.body.note}');`)
});

//query to delete supervisors according name
router.post('/deletename', function(req, res) {
 MyClient.query(`DELETE FROM main.mentor
                WHERE namementor = '${req.body.nameorid}' ;`
              ).then((result) => {res.send("המנחה נמחק בהצלחה")}, err => {res.send("המנחה מקושר לפרויקט - אין אפשרות למחוק ")})
});

//query to delete supervisors according id
router.post('/deleteid', function(req, res) {
      res.send(req.body.nameorid);
      MyClient.query(`DELETE FROM main.mentor
                      WHERE idmentor = '${req.body.nameorid}' ;`)
});

module.exports = router;
