var express = require('express');
var router = express.Router();
var MyClient = require('../connectionDB.js');
var Project = require('../GetTable/GetProject');
var Diploma = require('../GetTable/GetDiploma');
var Test = require('../GetTable/GetTest');
const R = require('ramda');

router.get('/testing',function(req, res){
      Test.getStartTable(function(err, result){
        res.send(result.rows)
      });
});

router.get('/testfilter',function(req, res){

  MyClient.query(`SELECT student.firstname || student.lastname as fullname, student.id, test_date.date, test.name_test
                  FROM main.student
                  JOIN main.student_test ON student.studentid = student_test.student_id
                  JOIN main.test_date ON student_test.test_symbol = test_date.test_symbol AND student_test.date = test_date.date
                  JOIN main.test ON test.test_symbol = test_date.test_symbol
                  WHERE test_date.test_symbol = '${req.body.test_symbol}' and test_date.date BETWEEN '${req.body.date_from}' AND '${req.body.date_until}';`)
                  .then((result) => {res.send(result.rows) ,console.log(result.rows)})



});



router.get('/project', function(req, res, next) {
        Project.getData(function(err, result){
                res.send(result.rows)
        });
});
router.get('/diploma', function(req, res, next) {
        Diploma.getData(function(err, result){
                res.send(result.rows)
        });
});
router.get('/', function(req, res, next) {
      Diploma.getData(function(err, result) {
          const a = result.rows.map((x,index)=>{
            const b = [index+10]
            return b.concat(R.values(x))
          })
          console.log("hay from home")
          res.send(a)
        });
});
router.get('/test', function(req, res, next) {
      Test.getStartTable(function(err, result){
          res.send(result.rows)
        });
});

router.get('/defense', function(req, res, next) {
      Diploma.getData(function(err, result) {
          const a = result.rows.map((x,index)=>{
            const b = [index+2]
            return b.concat(R.values(x))
          })
          console.log("hay from defense")
          res.send(a)
        });
});



module.exports = router;
